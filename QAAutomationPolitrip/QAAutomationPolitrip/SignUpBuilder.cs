﻿using OpenQA.Selenium;
using QAAutomationPolitrip.PageObjects;

namespace QAAutomationPolitrip.Steps
{
    public class SignUpBuilder
    {
        public static WebDriver driver;
        private SignUpPage _signUpPage = new SignUpPage(driver);

        public SignUpBuilder(WebDriver _driver, SignUpPage signUpPage)
        {
            driver = _driver; 
           _signUpPage = signUpPage;
        }

        public SignUpBuilder WithFirstName(string firstName)
        {
            _signUpPage.enterFirstName(firstName);
            return this;
        }

        public SignUpBuilder WithLastName(string lastName)
        {
            _signUpPage.enterLastName(lastName);
            return this;
        }

        public SignUpBuilder WithEmail(string email)
        {
            _signUpPage.enterEmail(email);
            return this;
        }

        public SignUpBuilder WithPassword(string password)
        {
            _signUpPage.enterPassword(password);
            return this;
        }

        public SignUpBuilder WithPasswordConfirmation(string passwordConfirmation)
        {
            _signUpPage.enterPasswordConfirmation(passwordConfirmation);
            return this;
        }
    }
}
