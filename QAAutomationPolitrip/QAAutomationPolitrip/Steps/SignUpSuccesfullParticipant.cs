﻿using NUnit.Framework;
using OpenQA.Selenium;
using QAAutomationPolitrip.PageObjects;
using System.Threading;
using TechTalk.SpecFlow;

namespace QAAutomationPolitrip.Steps
{
    [Binding]
    public class SignUpSuccesfullParticipant
    {
        private static WebDriver driver = SignUp.driver;
        public static SignUpPage _signUpPage = new SignUpPage(driver);
        private readonly HomePage _homePage = new HomePage(driver);
        private readonly string _messageString = "//*[text()=' Messages ']";

        [When(@"I press the Participant")]
        public void WhenIPressTheParticipant()
        {
            Thread.Sleep(1000);
            _signUpPage.clickOnParticipant();
        }

        [Then(@"I should be sign up successfully as Participant")]
        public void ThenIShouldBeSignUpSuccessfullyAsParticipant()
        {
            Assert.True(_homePage.checkIfTextIsPresentOnPage(_messageString, "Messages"));
            driver.Quit();
        }
    }
}
