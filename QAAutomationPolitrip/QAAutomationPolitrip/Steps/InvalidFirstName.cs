﻿using NUnit.Framework;
using OpenQA.Selenium;
using QAAutomationPolitrip.PageObjects;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using static QAAutomationPolitrip.Steps.SignUp;

namespace QAAutomationPolitrip.Steps
{
    [Binding]
    public class InvalidFirstName
    {
        private static WebDriver driver = SignUp.driver;
        public static SignUpPage _signUpPage = new SignUpPage(driver);
        private readonly SignUpBuilder _signUpBuilder = new SignUpBuilder(driver, _signUpPage);
        private SignUpDetails _signUpDetails;
        private readonly string _invalidFirstNameError = "//*[@id='sign-up-first-name-div']/app-form-control-error-message/em/span";

        [Given(@"I complete first name and last name")]
        public void GivenICompleteFirstNameAndLastName(Table signUpTable)
        {
            _signUpDetails = signUpTable.CreateInstance<SignUpDetails>();
            _signUpBuilder
                .WithFirstName(_signUpDetails.firstName)
                .WithLastName(_signUpDetails.lastName);
        }


        [Then(@"I should get an error message with Wrong characters or format")]
        public void ThenIShouldGetAnErrorMessageWithWrongCharactersOrFormat()
        {
            Assert.True(_signUpPage.checkIfTextIsPresentOnPage(_invalidFirstNameError, "Wrong characters or format"));
            driver.Quit();
        }

    }
}
