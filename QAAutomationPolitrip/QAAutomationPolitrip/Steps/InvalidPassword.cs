﻿using NUnit.Framework;
using OpenQA.Selenium;
using QAAutomationPolitrip.PageObjects;
using TechTalk.SpecFlow;

namespace QAAutomationPolitrip.Steps
{
    [Binding]
    public class InvalidPassword
    {
        private static WebDriver driver = SignUp.driver;
        public static SignUpPage _signUpPage = new SignUpPage(driver);
        private readonly string _invalidPasswordErrorMessage = "//*[@id='sign-up-password-div']/app-form-control-error-message/em/span";

        [Then(@"I should get an error message with invalid password")]
        public void ThenIShouldGetAnErrorMessageWithInvalidPassword()
        {
            Assert.True(_signUpPage.checkIfTextIsPresentOnPage(_invalidPasswordErrorMessage, "Password must contain at least 8 characters, 1 uppercase letter, 1 lowercase letter and 1 digit"));
            driver.Quit();
        }
    }
}
