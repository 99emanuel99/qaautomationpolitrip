﻿Feature: SignUpSuccesfullyOrganizer

Scenario: Sign Up Succesfully as Organizer
	Given I navigate to Sign Up page
	And I complete all the fields
		| FirstName | LastName | Email  | Password    | PasswordConfirmation |
		| Alexandru | Mihai    | alex54 | Password123 | Password123          |
	And I accept the cookies
	And I choose an element from the list
	When I press the sign up button
	And I press the Organizer
	Then I should be sign up successfully as Organizer