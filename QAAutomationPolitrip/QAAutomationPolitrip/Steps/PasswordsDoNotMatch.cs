﻿using NUnit.Framework;
using OpenQA.Selenium;
using QAAutomationPolitrip.PageObjects;
using TechTalk.SpecFlow;

namespace QAAutomationPolitrip.Steps
{
    [Binding]
    public class PasswordsDoNotMatch
    {
        private static WebDriver driver = SignUp.driver;
        public static SignUpPage _signUpPage = new SignUpPage(driver);
        private readonly string _passwordsDoNotMatch = "//*[@id='sign-up-confirm-password-div']/app-form-control-error-message/em/span";

        [Then(@"I should get an error message with passwords do not match")]
        public void ThenIShouldGetAnErrorMessageWithPasswordsDoNotMatch()
        {
            Assert.True(_signUpPage.checkIfTextIsPresentOnPage(_passwordsDoNotMatch, "Passwords must match"));
            driver.Quit();
        }
    }
}
