﻿Feature: SignUpSuccessfully
	
Scenario: Sign Up Successfully
	Given I navigate to Sign Up page
	And I complete all the fields
		| FirstName | LastName | Email   | Password    | PasswordConfirmation |
		| Alexandru | Mihai    | alex43c | Password123 | Password123          |
	And I accept the cookies
	And I choose an element from the list
	When I press the sign up button
	Then I should be sign up successfully