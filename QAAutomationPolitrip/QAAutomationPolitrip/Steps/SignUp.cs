﻿using OpenQA.Selenium;
using QAAutomationPolitrip.PageObjects;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace QAAutomationPolitrip.Steps
{
    [Binding]
    public class SignUp
    {
        public static WebDriver driver = Driver.LocalDriver();
        public static SignUpPage _signUpPage = new SignUpPage(driver);
        private readonly SignUpBuilder _signUpBuilder = new SignUpBuilder(driver, _signUpPage);
        private SignUpDetails _signUpDetails;
        private readonly HomePage _homePage = new HomePage(driver);

        [Given(@"I navigate to Sign Up page")]
        public void GivenINavigateToSignUpPage()
        {
            _signUpPage.navigateToSignUpPage();
        }

        [Given(@"I accept the cookies")]
        public void GivenIAcceptTheCookies()
        {
            _signUpPage.acceptCookie();
        }

        [Given(@"I complete all the fields")]
        public void GivenICompleteAllTheFields(Table signUpTable)
        {
            System.Random random = new System.Random();
            _signUpDetails = signUpTable.CreateInstance<SignUpDetails>();
            _signUpBuilder
                .WithFirstName(_signUpDetails.firstName)
                .WithLastName(_signUpDetails.lastName)
                .WithEmail(_signUpDetails.email + random.Next(50) + "@gmail.com")
                .WithPassword(_signUpDetails.password)
                .WithPasswordConfirmation(_signUpDetails.passwordConfirmation);
        }


        [Given(@"I choose an element from the list")]
        public void GivenIChooseAnElementFromTheList()
        {
            _signUpPage.clickWebSearch();
        }


        [Given(@"I press the sign up button")]
        public void GivenIPressTheSignUpButton()
        {
            _signUpPage.clickOnSignUpButton();
        }

        [When(@"I press the sign up button")]
        public void WhenIPressTheSignUpButton()
        {
            _signUpPage.clickOnSignUpButton();
        }

        [Then(@"I should be sign up successfully")]
        public void ThenIShouldBeSignUpSuccessfully()
        {
           _homePage.navigateToSignUpPage();
            driver.Quit();
        }

        public class SignUpDetails
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string email { get; set; }
            public string password { get; set; }
            public string passwordConfirmation { get; set; }
       
        }

    }
}
