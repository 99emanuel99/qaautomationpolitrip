﻿using NUnit.Framework;
using OpenQA.Selenium;
using QAAutomationPolitrip.PageObjects;
using TechTalk.SpecFlow;

namespace QAAutomationPolitrip.Steps
{
    [Binding]
    public class LogInSuccesfully
    {
        private static WebDriver driver = Driver.LocalDriver();
        private static LogInPage _logInPage = new LogInPage(driver);
        private readonly HomePage _homePage = new HomePage(driver);
        private readonly string _messageString = "//*[text()=' Messages ']";

        [Given(@"I navigate to Log in Page")]
        public void GivenINavigateToLogInPage()
        {
            _logInPage.navigateToLogInPage();
        }

        [Given(@"I accept the cookies on Log in page")]
        public void GivenIAcceptTheCookiesOnLogInPage()
        {
            _logInPage.acceptCookie();
        }


        [Given(@"I insert the correct credentials")]
        public void GivenIInsertTheCorrectCredentials()
        {
            _logInPage.logWithMyCredentials();
        }

        [When(@"I press the Log In button")]
        public void WhenIPressTheLogInButton()
        {
            _logInPage.clickOnLogInButton();
        }

        [Then(@"I should be redirected to a home page")]
        public void ThenIShouldBeRedirectedToAHomePage()
        {
            Assert.True(_homePage.checkIfTextIsPresentOnPage(_messageString, "Messages"));
            driver.Quit();
        }
    }

   

}
