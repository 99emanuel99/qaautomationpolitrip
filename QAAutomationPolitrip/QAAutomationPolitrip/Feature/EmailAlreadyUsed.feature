﻿Feature: EmailAlreadyUsed

Scenario: Check if I get an error when I try to create an account with an email already used
	Given I navigate to Sign Up page
	And I complete all the fields and an already used email
		| FirstName | LastName | Email               | Password    | PasswordConfirmation |
		| Razvan    | Mihai    | razvan123@yahoo.com | Password123 | Password123          |
	And I accept the cookies
	And I choose an element from the list
	When I press the sign up button
	Then I should get an error message because a user has already used this email 