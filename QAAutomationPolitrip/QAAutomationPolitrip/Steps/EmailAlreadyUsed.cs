﻿using NUnit.Framework;
using OpenQA.Selenium;
using QAAutomationPolitrip.PageObjects;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using static QAAutomationPolitrip.Steps.SignUp;


namespace QAAutomationPolitrip.Steps
{
    [Binding]
    public class EmailAlreadyUsed
    {
        private static WebDriver driver = SignUp.driver;
        public static SignUpPage _signUpPage = new SignUpPage(driver);
        private readonly SignUpBuilder _signUpBuilder = new SignUpBuilder(driver, _signUpPage);
        private SignUpDetails _signUpDetails;
        private readonly string _emailAlreadyUsedErrorMessage= "//*[@id='sign-up-error-div']";

        [Given(@"I complete all the fields and an already used email")]
        public void GivenICompleteAllTheFieldsAndAnAlreadyUsedEmail(Table signUpTable)
        {
            _signUpDetails = signUpTable.CreateInstance<SignUpDetails>();
            _signUpBuilder
                .WithFirstName(_signUpDetails.firstName)
                .WithLastName(_signUpDetails.lastName)
                .WithEmail(_signUpDetails.email)
                .WithPassword(_signUpDetails.password)
                .WithPasswordConfirmation(_signUpDetails.passwordConfirmation);
        }

        [Then(@"I should get an error message because a user has already used this email")]
        public void ThenIShouldGetAnErrorMessageBecauseAUserHasAlreadyUsedThisEmail()
        {
            Assert.True(_signUpPage.checkIfTextIsPresentOnPage(_emailAlreadyUsedErrorMessage, "An user with this email is already registered"), "Email validation error is not the same");
            driver.Quit();
        }

    }
}
