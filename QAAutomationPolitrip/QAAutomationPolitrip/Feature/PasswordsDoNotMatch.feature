﻿Feature: PasswordsDoNotMatch

Scenario: Check if a get an error if the passwords do not match
	Given I navigate to Sign Up page
	And I complete all the fields
		| FirstName | LastName | Email | Password    | PasswordConfirmation |
		| Alexandru | Mihai    | alex  | Password123 | Password1234         |
	And I accept the cookies
	And I choose an element from the list
	When I press the sign up button
	Then I should get an error message with passwords do not match

