﻿Feature: InvalidFirstName

Scenario: Check if I get an error when I try to use a first name with wrong character of format
	Given I navigate to Sign Up page
	And I complete first name and last name 
			| FirstName    | LastName |
			| Alexandru423 | Mihai    |         
	Then I should get an error message with Wrong characters or format