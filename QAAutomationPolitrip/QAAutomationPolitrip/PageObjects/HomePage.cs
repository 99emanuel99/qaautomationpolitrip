﻿using OpenQA.Selenium;
using System;

namespace QAAutomationPolitrip.PageObjects
{
    public class HomePage 
    {
        public WebDriver driver;
        public HomePage(WebDriver driver ){ this.driver = driver; }

        private readonly By _homeButton = By.XPath("//a[@id='qa_header-home']/span[@class='label']");
        private readonly By _howItWorks = By.XPath("//a[@id='qa_header-how-it-works']/span[@class='label']");
        private readonly By _logIn = By.XPath("/html//a[@id='qa_header-login']");
        private readonly By _signUp = By.XPath("/html//a[@id='qa_header-signup']");
        private readonly By _messageButton = By.XPath("//*[contains(text(),'Messages')]");
        public readonly string _messageText = "//*[text() = ' Messages ']";

        public void clickOnHome()
        {
            driver.FindElement(_homeButton).Click();
        }
        public void clickOnHowItWork()
        {
            driver.FindElement(_howItWorks).Click();
        }

        public void clickOnSignUp()
        {
            driver.FindElement(_signUp).Click();
        }

        public void clickOnLogIn()
        {
            driver.FindElement(_logIn).Click();
        }
        public void clickOnMessage()
        {
            driver.FindElement(_messageButton).Click();
        }

        public void navigateToSignUpPage()
        {
            driver.Navigate().GoToUrl("https://politrip.com/");
        }

        public bool checkIfTextIsPresentOnPage(string pathToElement, string expectedText)
        {
            string nameOfElement = this.driver.FindElement(By.XPath(pathToElement)).Text;
            if (nameOfElement.Equals(expectedText))
                return true;
            return false;

        }
    }
}
