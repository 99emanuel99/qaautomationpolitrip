﻿Feature: LogInSuccessfully

Scenario: Log In Succesfully
	Given I navigate to Log in Page
	And I insert the correct credentials
	And I accept the cookies on Log in page
	When I press the Log In button
	Then I should be redirected to a home page