﻿Feature: InvalidLastName

Scenario: Check if I get an error when I try to use a last name with wrong character of format
	Given I navigate to Sign Up page
	And I complete first name, last name and email
			| FirstName | LastName  | Email |
			| Alexandru | Mihai3145 | alex  |
	Then I should get an error message with Wrong characters or format for last name