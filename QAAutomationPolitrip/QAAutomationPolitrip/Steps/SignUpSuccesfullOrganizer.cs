﻿using NUnit.Framework;
using OpenQA.Selenium;
using QAAutomationPolitrip.PageObjects;
using System.Threading;
using TechTalk.SpecFlow;

namespace QAAutomationPolitrip.Steps
{
    [Binding]
    public class SignUpSuccesfullOrganizer
    {
        private static WebDriver driver = SignUp.driver;
        public static SignUpPage _signUpPage = new SignUpPage(driver);
        private readonly HomePage _homePage = new HomePage(driver);
        private readonly string _messageString = "//*[text()=' Messages ']";


        [When(@"I press the Organizer")]
        public void WhenIPressTheOrganizer()
        {
            Thread.Sleep(1000);
            _signUpPage.clickOnOrganizer();
        }

        [Then(@"I should be sign up successfully as Organizer")]
        public void ThenIShouldBeSignUpSuccessfullyAsOrganizer()
        {
            Assert.True(_homePage.checkIfTextIsPresentOnPage(_messageString, "Messages"));
            driver.Quit();
        }

    }
}
