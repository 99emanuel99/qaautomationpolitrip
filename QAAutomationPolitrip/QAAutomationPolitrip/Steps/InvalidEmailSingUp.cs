﻿using NUnit.Framework;
using OpenQA.Selenium;
using QAAutomationPolitrip.PageObjects;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using static QAAutomationPolitrip.Steps.SignUp;

namespace QAAutomationPolitrip.Steps
{
    [Binding]
    public class InvalidEmailSingUp
    {
        private static WebDriver driver = SignUp.driver;
        public static SignUpPage _signUpPage = new SignUpPage(driver);
        private readonly SignUpBuilder _signUpBuilder = new SignUpBuilder(driver, _signUpPage);
        private SignUpDetails _signUpDetails;
        private readonly string _invalidEmailSignInErrorMessage = "//*[@id='sign-up-email-div']/app-form-control-error-message/em/span";


        [Given(@"I complete all the fields with invalid email")]
        public void WhenICompleteAllTheFieldsWithInvalidEmail(Table signUpTable)
        {
            _signUpDetails = signUpTable.CreateInstance<SignUpDetails>();
            _signUpBuilder
                .WithFirstName(_signUpDetails.firstName)
                .WithLastName(_signUpDetails.lastName)
                .WithEmail(_signUpDetails.email)
                .WithPassword(_signUpDetails.password)
                .WithPasswordConfirmation(_signUpDetails.passwordConfirmation);
        }

        [Then(@"I should get an error message with invalid email address")]
        public void ThenIShouldGetAnErrorMessageWithInvalidEmailAddress()
        {
            Assert.True(_signUpPage.checkIfTextIsPresentOnPage(_invalidEmailSignInErrorMessage, "Please enter a valid email address"));
            driver.Quit();
        }
    }
}
