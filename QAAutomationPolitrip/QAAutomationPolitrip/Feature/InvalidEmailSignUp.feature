﻿Feature: InvalidEmailSignUp

Scenario:  Check if I get an error when I try to use an invalid email
	Given I navigate to Sign Up page
	And I complete all the fields with invalid email
			| FirstName | LastName | Email | Password    | PasswordConfirmation |
			| Alexandru | Mihai    | alexyahoo.com  | Password123 | Password123          |
	And I accept the cookies
	And I choose an element from the list
	When I press the sign up button
	Then I should get an error message with invalid email address