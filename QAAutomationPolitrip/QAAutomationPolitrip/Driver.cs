﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace QAAutomationPolitrip
{
    public class Driver
    {
        public static WebDriver LocalDriver()
        {
            var chromeDriver = new ChromeDriver();

            chromeDriver.Manage().Window.Maximize();
            chromeDriver.Manage().Cookies.DeleteAllCookies();
            chromeDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
  
            return chromeDriver;
        }
    }
}
