﻿Feature: InvalidPassword

Scenario: Check if I get an error when I try to use an invalid password
	Given I navigate to Sign Up page
	And I complete all the fields
		| FirstName | LastName | Email | Password | PasswordConfirmation |
		| Alexandru | Mihai    | alex  | password | password             |
	And I accept the cookies
	And I choose an element from the list
	When I press the sign up button
	Then I should get an error message with invalid password