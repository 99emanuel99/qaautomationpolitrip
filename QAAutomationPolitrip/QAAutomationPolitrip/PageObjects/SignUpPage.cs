﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace QAAutomationPolitrip.PageObjects
{
    public class SignUpPage 
    {
        public WebDriver driver;
        public SignUpPage(WebDriver driver) 
        { this.driver = driver; }


        private readonly By _firstName = By.XPath("//input[@name='first-name']");
        private readonly By _lastName = By.XPath("//input[@name='last-name']");
        private readonly By _email = By.XPath("//input[@name='email']");
        private readonly By _password = By.XPath("//input[@placeholder='Enter your password']");
        private readonly By _passwordConfirm = By.XPath("//input[@placeholder='Repeat your password']");
        private readonly By _buttonSignUp = By.CssSelector("div > button");
        private readonly By _buttonAcceptCookies = By.XPath("//div[@id='cookiescript_accept']");
        private readonly By _listWebSearch = By.XPath("//option[text() = 'Web-Search']");
        private readonly By _listSocialNetworks = By.XPath("//option[text() = 'Social networks']");
        private readonly By _listFromAFriend = By.XPath("//option[text() = 'From a friend']");
        private readonly By _listOther = By.XPath("//option[text() = 'Other']");
        private readonly By _termsAndConditions = By.XPath("//*[@id='sign-up-component-div']/div/p[1]");
        private readonly By _footer= By.XPath("/html/body/app-root/app-access-account/app-page-template/div/app-footer/footer");
        private readonly By _buttonParticipant = By.XPath("//*[@id='qa_signup-participant']");
        private readonly By _buttonOrganizer= By.XPath("//*[@id='qa_signup-organizer']");

        public void clickOnSignUpButton()
        {
            driver.FindElement(_buttonSignUp).Click();
        }

        public void acceptCookie()
        {
            driver.FindElement(_buttonAcceptCookies).Click();
        }


        public void navigateToSignUpPage()
        {
            driver.Navigate().GoToUrl("https://politrip.com/account/sign-up");

        }

        public void clickWebSearch() 
        {
            driver.FindElement(_listWebSearch).Click();
        }

        public void clickSocialNetworks()
        {
            driver.FindElement(_listSocialNetworks).Click();
        }

        public void clickFromAFriend()
        {
            driver.FindElement(_listFromAFriend).Click();
        }

        public void clickOther()
        {
            driver.FindElement(_listOther).Click();
        }

        public void enterFirstName(string firstName)
        {
            driver.FindElement(_firstName).Clear();
            driver.FindElement(_firstName).SendKeys(firstName);
        }

        public void enterLastName(string lastName)
        {
            driver.FindElement(_lastName).Clear();
            driver.FindElement(_lastName).SendKeys(lastName);
        }

        public void enterEmail(string email)
        {
            driver.FindElement(_email).Clear();
            driver.FindElement(_email).SendKeys(email);
        }

        public void enterPassword(string password)
        {
            driver.FindElement(_password).Clear();
            driver.FindElement(_password).SendKeys(password);
        }

        public void enterPasswordConfirmation(string passwordConfirmation)
        {
            driver.FindElement(_passwordConfirm).SendKeys(passwordConfirmation);
        }


        public void clickOnParticipant()
        {
            Actions actions = new Actions(driver);
            actions.MoveToElement(driver.FindElement(_termsAndConditions));
            actions.ClickAndHold();
            actions.MoveToElement(driver.FindElement(_buttonOrganizer));
            actions.Release().Perform();
            driver.FindElement(_buttonParticipant).Click();
        }

        public void clickOnOrganizer()
        {
            Actions actions = new Actions(driver);
            actions.MoveToElement(driver.FindElement(_termsAndConditions));
            actions.ClickAndHold();
            actions.MoveToElement(driver.FindElement(_footer));
            actions.Release().Perform();
            driver.FindElement(_buttonOrganizer).Click();
        }

        public bool checkIfTextIsPresentOnPage(string pathToElement, string expectedText)
        {
            string nameOfElement = this.driver.FindElement(By.XPath(pathToElement)).Text.ToString();
            if (nameOfElement.Equals(expectedText))
                return true;
            return false;

        }

    }

}
