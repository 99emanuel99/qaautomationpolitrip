﻿using NUnit.Framework;
using OpenQA.Selenium;
using QAAutomationPolitrip.PageObjects;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using static QAAutomationPolitrip.Steps.SignUp;

namespace QAAutomationPolitrip.Steps
{
    [Binding]
    public class InvalidLastName
    {
        private static WebDriver driver = SignUp.driver;
        public static SignUpPage _signUpPage = new SignUpPage(driver);
        private readonly SignUpBuilder _signUpBuilder = new SignUpBuilder(driver, _signUpPage);
        private SignUpDetails _signUpDetails;
        private readonly string _invalidLastNameError= "//*[@id='sign-up-last-name-div']/app-form-control-error-message/em/span";

        [Given(@"I complete first name, last name and email")]
        public void GivenICompleteFirstNameLastNameAndEmail(Table signUpTable)
        {
            System.Random random = new System.Random();
            _signUpDetails = signUpTable.CreateInstance<SignUpDetails>();
            _signUpBuilder
                .WithFirstName(_signUpDetails.firstName)
                .WithLastName(_signUpDetails.lastName)
                .WithEmail(_signUpDetails.email + random.Next(50) + "@gmail.com");
        }

        [Then(@"I should get an error message with Wrong characters or format for last name")]
        public void ThenIShouldGetAnErrorMessageWithWrongCharactersOrFormatForLastName()
        {
            Assert.True(_signUpPage.checkIfTextIsPresentOnPage(_invalidLastNameError, "Wrong characters or format"));
            driver.Quit();
        }



    }
}
