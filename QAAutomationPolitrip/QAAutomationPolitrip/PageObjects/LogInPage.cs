﻿using OpenQA.Selenium;

namespace QAAutomationPolitrip.PageObjects
{

    public class LogInPage 
    {
        public WebDriver driver;
        public LogInPage(WebDriver driver) { this.driver = driver; }


        private readonly By _emailLogIn = By.XPath("//input[@id='login-email-input']");
        private readonly By _passwordLogIn = By.XPath("//input[@id='login-password-input']");
        private readonly By _buttonLogIn = By.CssSelector("div > button");
        private readonly By _buttonAcceptCookie = By.XPath("//*[@id='cookiescript_accept']");

        public void clickOnLogInButton()
        {
            driver.FindElement(_buttonLogIn).Click();
        }

        public void navigateToLogInPage()
        {
            driver.Navigate().GoToUrl("https://politrip.com/account/login");

        }

        public void acceptCookie() 
        {
            driver.FindElement(_buttonAcceptCookie).Click();
        }


        public void logWithMyCredentials()
        {
            driver.FindElement(_emailLogIn).SendKeys("alex@yahoo.com");
            driver.FindElement(_passwordLogIn).SendKeys("Password123");
        }
    }
}
